# Got idea for this script from https://community.zscaler.com/t/installing-tls-ssl-root-certificates-to-non-standard-environments/7261

function Enter-Home {
    ### Go Home ###
    Write-Output "--> Change directory into users' home directory."
    Set-Location ~
}

function Remove-Cert-Files($filelist) {
    ### Cleanup ###
    Write-Output "--> Clean up temporary cert files."
    foreach ($i in $filelist) {
        Remove-Item $i
    }
}

function Test-Privileges {
    Write-Host '--> Checking for elevated permissions.'
    if (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) {
        Write-Warning 'Insufficient permissions to run this script. Open the PowerShell console as an administrator and run this script again.'
        Write-Warning 'In Powershell issue command: powershell Start-Process powershell -Verb runAs -FilePath <path to this ps1 file>'
        Break
    }
}

function Get-Zscaler-Cert($certname) {
    Write-Output "--> Extract Zscaler Root CA certificate from Windows cert store."
    ### Extract ZscalerRootCertificate from Cert Store ###
    $thumbprint = (Get-ChildItem -Path Cert:\LocalMachine\Root | Where-Object {$_.Subject -match "CN=Zscaler Root CA"}).Thumbprint
    $certpath = 'Cert:\LocalMachine\Root\' + $thumbprint
    Export-Certificate -Cert $certpath -FilePath $certname
}

function Convert-Cert($certname, $penname) {
    ### Convert to PEM ###
    certutil.exe -encode $certname $pemname
}

function Add-Cert-To-Git($penname) {
    if (Get-Command "git.exe" -ErrorAction SilentlyContinue) {
        Write-Output "--> Add Zscaler cert to git cert store."
        Get-Content $pemname | Add-Content $(git config --get http.sslcainfo)
    }
    else {
        Write-Output "git is either not installed or not in the %PATH%.  Skipping."
    }
}

function Add-Cert-To-Python($penname) {
    if (Get-Command "python.exe" -ErrorAction SilentlyContinue) {
        $pythonstartpath = Split-Path (Get-Command "python.exe").Path -Parent

        $pythonendpath1 = '\Lib\site-packages\pip\_vendor\certifi\cacert.pem'
        $pythonendpath2 = '\Lib\site-packages\certifi\cacert.pem'

        Write-Output "--> Add Zscaler cert to python cert store."
        $pythonpath = $pythonstartpath + $pythonendpath1
        Get-Content $pemname | Add-Content $pythonpath
        $pythonpath = $pythonstartpath + $pythonendpath2
        Get-Content $pemname | Add-Content $pythonpath
    }
    else {
        Write-Output "python is either not installed or not in the %PATH%.  Skipping."
    }
}

function Main {
    Write-Output "--> Program Start."
    $certname = 'ZscalerRootCertificate.cer'
    $pemname = 'ZscalerRootCertificate.pem'
    
    Enter-Home

    Get-Zscaler-Cert $certname
    Convert-Cert $certname $penname

    # If cert files exist do importation to 3rd party applications.
    if ((Test-Path -Path $PWD\$certname -PathType Leaf) -And (Test-Path -Path $PWD\$pemname -PathType Leaf)) {
        Write-Output "--> Cert extraction and conversion successful."

        Add-Cert-To-Git $penname
        Add-Cert-To-Python $penname

        # Cleanup
        Remove-Cert-Files $certname, $pemname
    }
    Else {
        Write-Output "--> Extracted cert files not in expected location.  Please check home directory for files named: " + $certname + " and " + $pemname + "."
    }

    Write-Output "--> Program Done."
}

# Run Program
Test-Privileges
Main
