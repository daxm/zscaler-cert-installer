#!/usr/bin/bash

# I exported the Zscaler certifcate out of Microsoft Cert Manager.  It was located under 'Trusted Root Certification > Certificates' as zscaler_cert.cer.
# Though the extension is '.cer' it really is a DER formatted file.  (Thanks Microsoft!)
# I then copied that file into Ubuntu running in WSL.

# Convert DER encoded file to CRT.
openssl x509 -inform DER -in zscaler_cert.cer -out zscaler_cert.crt

# Move the CRT file to /usr/local/share/ca-certificates
sudo mv zscaler_cert.crt /usr/local/share/ca-certificates

# Inform Ubuntu of new cert.
sudo update-ca-certificates
