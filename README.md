# Zscaler Cert Installer

Script to import the Zscaler cert into programs that utilize their own private cert stores.

## Windows
The current Windows installer only deals with 'git' and 'python'.  I've only ever tested it on 1 Windows machine so your mileage may vary.

### TODOs
* Check whether cert is already added prior to adding (right now it will just append cert to bottom of file over and over)
* Support, somehow, Python virtual environments.

## Ubuntu
1. Get the DER encoded cert and name it zscaler_cert.cer (WIndows cert export defaults to cer as the extension) and put it in the same directory as the script.
1. Ensure the script is executable: `chmod 755 Zscaler_Cert_Installer_for_Ubuntu.sh`
1. Run the script: `sudo ./Zscaler_Cert_Installer_for_Ubuntu.sh`
